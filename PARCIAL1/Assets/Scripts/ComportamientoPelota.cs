﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoPelota : MonoBehaviour
{
    public GameObject explosion_turquesa;
    public GameObject explosion_verde;
    public GameObject explosion_violeta;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnCollisionEnter(Collision col)
    {
        #region Colision contra enemigos

        
        if (col.gameObject.name.Contains("Turquesa"))
        {
            GestordeAudio.instancia.PausarSonido("musica");
            GestordeAudio.instancia.ReproducirSonido("perder");
            Destroy(this.gameObject);
            GameObject particulas = Instantiate(explosion_turquesa, transform.position, Quaternion.identity) as GameObject;
            ListaObjetos.instancia.RestartGame(2f);
        }

        if (col.gameObject.name.Contains("Verde"))
        {
            GestordeAudio.instancia.PausarSonido("musica");
            GestordeAudio.instancia.ReproducirSonido("perder");
            Destroy(this.gameObject);
            GameObject particulas = Instantiate(explosion_verde, transform.position, Quaternion.identity) as GameObject;
            ListaObjetos.instancia.RestartGame(2f);
        }

        if (col.gameObject.name.Contains("Violeta"))
        {
            GestordeAudio.instancia.PausarSonido("musica");
            GestordeAudio.instancia.ReproducirSonido("perder");
            Destroy(this.gameObject);
            GameObject particulas = Instantiate(explosion_violeta, transform.position, Quaternion.identity) as GameObject;
            ListaObjetos.instancia.RestartGame(2f);
        }
        #endregion//acá se controla la colisión contra los 3 enemigos y los eventos que desencadena la misma
    }

    private void OnTriggerEnter(Collider obj)//detecta cuando la pelota pasa por un objeto vacío que tiene como tag victoria, para desatar el evento de victoria
    {
        if(obj.tag=="victoria")
        {
            ListaObjetos.instancia.victoria.enabled = true;
            GestordeAudio.instancia.PausarSonido("musica");
            GestordeAudio.instancia.ReproducirSonido("ganar");
        }
    }
}
