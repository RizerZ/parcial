﻿using UnityEngine;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour//clase que controla al jugador
{
    #region Propiedades

   
    public float Rapidez=8.0f;//propiedad para movimiento
    Rigidbody rb;//rigidbody del personaje
    Collider coll;//collider del personaje, creo q no lo uso al final...
    public float magnitudSalto=20;//propiedad para salto
    bool coleccionable = false;//propiedad para saber si recolectó el coleccionable
    public int vida = 100;//propiedad para manejar la vida
    

    int saltoActual = 0;//propiedad para regular la cantidad de saltos
    bool enElSuelo = true;//propiedad para saber si está en el suelo o no y de esta forma controlar los saltos
    int maxSaltos = 2;//máximo de saltos
    #endregion
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        coll = GetComponent<Collider>();//obtengo los componentes rigidbody y collider del personaje
        Cursor.lockState = CursorLockMode.Locked;//saco el cursor para que no moleste
    }

    void Update()
    {
        movimiento();//llamo al método movimiento en cada frame
        salto();//llamo al método salto en cada frame
        if (Input.GetKeyDown("r"))// si se presiona la letra R se resetea la partida
        {
            
            ListaObjetos.instancia.RestartGame();//funcion declarada en el "GameManager", o sea... ListaObjetos
        }
        if(transform.position.y<-3)//si el personaje se cae el juego se resetea
        {
            GestordeAudio.instancia.ReproducirSonido("perder");
            ListaObjetos.instancia.RestartGame(2);//método con sobrecarga para resetear el juego
        }
        ListaObjetos.instancia.TextoVida(vida);//actualizo la vida
        ListaObjetos.instancia.TextoColeccionable(coleccionable);//actualizo el coleccionable (si fue agarrado o no)

        if(vida<=0)//cuando vida igual o menor a 0 sale el cartel de derrota, la música y se resetea el juego a los 2 segundos
        {
            ListaObjetos.instancia.derrota.enabled = true;
            GestordeAudio.instancia.ReproducirSonido("perder");
            ListaObjetos.instancia.RestartGame(2);
        }

    }

    void movimiento()//método movimiento
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * Rapidez;
        float movimientoCostados = Input.GetAxis("Horizontal") * Rapidez;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }
    void salto()//método salto
    {
       

        if(Input.GetButtonDown("Jump")&&(enElSuelo||maxSaltos>saltoActual))
        {
            GestordeAudio.instancia.ReproducirSonido("salto");
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            enElSuelo = false;
            saltoActual++;
        }
    }


    private void OnCollisionEnter(Collision col)//detector de colisiones
    {
        enElSuelo = false;
        saltoActual=0;
        if (col.gameObject.name.Contains("Turquesa"))
        {
            GestordeAudio.instancia.ReproducirSonido("tocarenemigo");
            vida -= 10;
        }

        if (col.gameObject.name.Contains("Verde"))
        {
            GestordeAudio.instancia.ReproducirSonido("tocarenemigo");
            vida -= 10;
        }

        if (col.gameObject.name.Contains("Violeta"))
        {
            GestordeAudio.instancia.ReproducirSonido("tocarenemigo");
            vida -= 10;
        }
        if(col.gameObject.name.Contains("Coleccionable"))//detecta la colisión con el coleccionable y lo destruye, además de darle las mejoras al jugador
        {

            Destroy(col.gameObject);
            Rapidez += 7;

            rb.mass = 10;
            magnitudSalto = 40;
            coleccionable = true;
            GestordeAudio.instancia.ReproducirSonido("powerup");
            

        }
    }

}

