﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoVioleta : MonoBehaviour
{
    public float velocidadCaida;
    float posicionInicial;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, velocidadCaida);//maneja el movimiento del enemigo
        if (transform.position.z > 5)
        {
            velocidadCaida *= -1;
        }
        if (transform.position.z < -5)
        {
            velocidadCaida *= -1;
        }
    }
}
