﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ListaObjetos : MonoBehaviour//esto vendría a ser un instanciador+gamemanager+gestor de textos
{
    public GameObject[] prefabs;
    public static ListaObjetos instancia;// propiedad publica y estática para poder llamarla desde otras clases
    public Text vida;
    public Text coleccionable;
    public Text tip1;
    public RawImage victoria;
    public RawImage derrota;


    // Start is called before the first frame update

    private void Awake()
    {
        if (instancia==null)
        {
            instancia = this;
        }
        victoria.enabled = false;
        derrota.enabled = false;
    }

    void Start()
    {
        for (int i = 0; i < prefabs.Length; i++)
        {
            Instantiate(prefabs[i]);
        }
        Tips();
        GestordeAudio.instancia.ReproducirSonido("musica");

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RestartGame()//declaración del método RestartGame
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void RestartGame(float segundos)//sobrecarga del método con segundos
    {
        Invoke("RestartGame", segundos);
    }

    public void TextoVida(int valor)//método que cambia el color de la vida cuando es menor o igual a 30
    {
        vida.text = ("Vida: " + valor);
        if(valor<=30)
        {
            vida.color = Color.red;
        }
    }
    public void TextoColeccionable(bool obtenido)//cuando el coleccionable es recolectado cambia el texto y el color
    {
        if(obtenido==true)
        {
            coleccionable.color = UnityEngine.Color.yellow;
            coleccionable.text = ("Power Up " + "SI");
        }
        else
        {
            coleccionable.text = ("Power Up " + "NO");
        }
    }
    public void Tips()//método que controla los tips
    {
        Invoke("SacarTip", 5);
    }
    public void SacarTip()//método que controla los tips
    {
        tip1.text = "";
    }
}
