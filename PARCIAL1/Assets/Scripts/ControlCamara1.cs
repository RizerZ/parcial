﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara1 : MonoBehaviour//camara en tercera persona de lejos con clamp
{
    Vector2 MouseMirar;
    Vector2 SuavidadV;

    public float Sensibilidad = 3.0f;
    public float Suavizado = 2.0f;

    GameObject Jugador;
    // Start is called before the first frame update
    void Start()
    {
        Jugador = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(Sensibilidad * Suavizado, Sensibilidad * Suavizado));
        SuavidadV.x = Mathf.Lerp(SuavidadV.x, md.x, 1f / Suavizado);
        SuavidadV.y = Mathf.Lerp(SuavidadV.y, md.y, 1f / Suavizado);

        MouseMirar += SuavidadV;

        MouseMirar.y = Mathf.Clamp(MouseMirar.y, -90f, 90f);

        transform.localRotation = Quaternion.AngleAxis(MouseMirar.y*-1, Vector3.right);
        Jugador.transform.localRotation = Quaternion.AngleAxis(MouseMirar.x, Jugador.transform.up);

    }
}
