﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoVerde : MonoBehaviour
{
    public float velocidadCaida;
    float posicionInicial;
    // Start is called before the first frame update
    void Start()
    {
        posicionInicial = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(velocidadCaida, 0, 0);
        if (transform.position.x-posicionInicial > 8)//maneja el movimiento del enemigo
        {
            velocidadCaida *= -1;
        }
        if (transform.position.x+posicionInicial < 2)
        {
            velocidadCaida *= -1;
        }
    }
}
