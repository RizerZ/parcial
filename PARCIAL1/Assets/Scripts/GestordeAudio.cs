﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestordeAudio : MonoBehaviour
{
    public Sonidos[] sonidos;
    public static GestordeAudio instancia;
    // Start is called before the first frame update
    private void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sonidos s in sonidos)
        {
            s.FuenteAudio = gameObject.AddComponent<AudioSource>();
            s.FuenteAudio.clip = s.ClipSonido;
            s.FuenteAudio.volume = s.Volumen;
            s.FuenteAudio.pitch = s.Pitch;
            s.FuenteAudio.loop = s.Repetir;
        }
    }
    public void ReproducirSonido(string nombre)
    {
        Sonidos s = Array.Find(sonidos, x => x.Nombre == nombre);
        if(s==null)
        {
            Debug.Log("Sonido " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Play();
        }
    }
    public void PausarSonido(string nombre)
    {
        Sonidos s = Array.Find(sonidos, x => x.Nombre == nombre);
        if (s == null)
        {
            Debug.Log("Sonido " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Pause();
        }
    }
}
