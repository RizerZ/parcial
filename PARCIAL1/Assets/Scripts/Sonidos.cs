﻿using UnityEngine;
[System.Serializable]

public class Sonidos
{
    public string Nombre;
    public AudioClip ClipSonido;
    [Range(0f, 1f)]
    public float Volumen;

    [Range(0f, 1f)]
    public float Pitch;
    public bool Repetir;

    [HideInInspector]
    public AudioSource FuenteAudio;
}
